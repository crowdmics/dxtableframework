//
//  DXTKBaseHeader.h
//  DXTableKit
//
//  Created by Vlad Korzun on 18.03.13.
//  Copyright (c) 2013 111min. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DXTKHeaderFooterFilling.h"

@interface DXTKBaseHeader : UITableViewHeaderFooterView<DXTKHeaderFooterFilling>

@end
