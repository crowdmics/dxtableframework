//
//  GridDataSource.h
//  Grid
//
//  Created by zen on 2/18/13.
//  Copyright (c) 2013 111min. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DXTKBaseDataSource.h"

@interface DXTKCollectionViewDataSource : DXTKBaseDataSource <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@end
